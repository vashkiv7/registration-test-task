import {BrowserRouter} from "react-router-dom";
import Router from "./router";
import './styles.css'

const App = () => {
  return (
      <BrowserRouter>
          <div className="page">
              <Router/>
          </div>
      </BrowserRouter>
  );
}

export default App;
