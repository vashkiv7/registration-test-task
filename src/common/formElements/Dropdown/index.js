import React, {useRef, useState} from 'react';
import styles from '../form.module.scss'
import arrowIcon from '../../../assets/img/arrow_drop_down.svg'
import useOutsideClick from "../../../hooks/useOutsideClick";

const Dropdown = ({
                      placeholder,
                      items= [],
                      onChange,
                      label,
                      value,
                      name,
                      required,
                      dropdownClassName,
                      headClassName,
                      bodyClassName,
                      headTitle = "title"
}) => {
    const [isDropdownShown, setIsDropdownShown] = useState(false)
    const dropdownRef = useRef(null)

    useOutsideClick(dropdownRef, () => setIsDropdownShown(false))

    const onItemClick = item => {
        onChange({
            target: {
                name: name,
                value: item
            }
        })

        setIsDropdownShown(false)
    }

    return (
        <div ref={dropdownRef} className={`${styles.dropdown} ${styles.form_item} ${dropdownClassName ? dropdownClassName : ""} ${isDropdownShown ? styles.opened : ""}`}>
            <label>{label}{required && <span>*</span>}</label>
            <div className={`${styles.dropdown_head} ${headClassName ? headClassName : ""} ${!!value ? styles.selected : ""}`}
                 onClick={() => setIsDropdownShown(!isDropdownShown)}
                 tabIndex={0}
            >
                {value?.[headTitle] ?? placeholder} <img src={arrowIcon} className={isDropdownShown ? styles.active : ""} alt=""/>
            </div>
            {
                isDropdownShown &&
                <div className={`${styles.dropdown_body} ${bodyClassName ? bodyClassName : ""}`}>
                    {
                        items.map((el,idx) => {
                            return (
                                    <div className={styles.dropdown_body_item}
                                         key={idx}
                                         onClick={() => onItemClick(el)}
                                    >{el?.title}</div>
                            )
                        })
                    }
                </div>
            }
        </div>
    );
};

export default Dropdown;