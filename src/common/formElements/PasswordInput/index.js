import React, {useState} from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import styles from '../form.module.scss'
import {EyeCrossedIcon, EyeIcon, KeyIcon, Copy, Retry} from "./icons";
import error_icon from '../../../assets/img/cross.svg'
import check_icon from '../../../assets/img/check.svg'

const PasswordInput = ({
                           label = "",
                           value = "",
                           name = "",
                           placeholder = "",
                           id,
                           isGenerator = true,
                           onChange = () => null,
                           error= null,
                           isPasswordGood = false
}) => {

    const [isPasswordShown, setIsPasswordShown] = useState(false)
    const [isGeneratorOpen, setIsGeneratorOpen] = useState(false)
    const [randomPassword, setRandomPassword] = useState(null)

    const getRandomPassword = () => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < 14) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        setRandomPassword(result)
    }

    const toggleShowPassword = () => setIsPasswordShown(!isPasswordShown)

    const onGeneratorClick = () => {
        getRandomPassword()
        setIsGeneratorOpen(!isGeneratorOpen)
    }

    const applyGeneratedPassword = () => {
        onChange({
            target: {
                name: "password",
                value: randomPassword
            }
        })
        onChange({
            target: {
                name: "passwordConfirm",
                value: randomPassword
            }
        })

        setIsGeneratorOpen(false)
    }

    const Generator = () => {
        return (
            <div className={styles.generator}>
                <div className={styles.generator_head}>
                    <span>{randomPassword}</span>
                    <div className={styles.generator_head_control}>
                        <div>
                            <CopyToClipboard text={randomPassword}>
                                <span>
                                    <Copy/>
                                </span>
                            </CopyToClipboard>
                        </div>
                        <div onClick={getRandomPassword}>
                            <Retry/>
                        </div>
                    </div>
                </div>
                <div className={styles.generator_apply}
                     onClick={applyGeneratedPassword}
                >Применить сгенерированный пароль</div>
            </div>
        )
    }

    return (
        <div className={styles.form_item}>
            <label htmlFor={id}>{label}</label>
            <div className={`${styles.password_input} ${isGeneratorOpen ? styles.active : ""}`}>
                <input
                    value={value}
                    id={id}
                    type={isPasswordShown ? "text" : "password"}
                    placeholder={placeholder}
                    onChange={onChange}
                    name={name}
                />
                <div className={styles.password_input_control}>
                    {
                        isGenerator &&
                        <div
                            className={`${styles.password_input_control_key} ${isGeneratorOpen ? styles.active : ""}`}
                            onClick={onGeneratorClick}
                        >
                            <KeyIcon/>
                        </div>
                    }
                    <div onClick={toggleShowPassword}>
                        {isPasswordShown ? <EyeIcon/> : <EyeCrossedIcon/>}
                    </div>
                </div>
            </div>
            {
                isGeneratorOpen && <Generator/>
            }
            {
                error &&
                    <div className={`${styles.password_status} ${styles.error}`}>
                        <img src={error_icon} alt=""/> <span>{error}</span>
                    </div>
            }
            {
                isPasswordGood && <div className={`${styles.password_status} ${styles.good}`}>
                    <img src={check_icon} alt=""/> <span>Пароль должен состоять из букв латинского алфавита (A-z), арабских цифр (0-9)</span>
                </div>
            }
        </div>
    );
};

export default PasswordInput;