import React from 'react';
import styles from '../form.module.scss'

const SubmitButton = ({type, text, disabled, className = "", btnStyle = "main", onClick}) => {
    return (
        <button
            type={type}
            disabled={disabled}
            className={`${styles.submit} ${styles[btnStyle]} ${className}`}
            onClick={onClick}
        >
            {text}
        </button>
    );
};

export default SubmitButton;