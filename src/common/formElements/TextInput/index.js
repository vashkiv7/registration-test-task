import React from 'react';
import styles from '../form.module.scss'

const TextInput = ({
                       label = "",
                       value = "",
                       name = "",
                       type = "text",
                       placeholder = "",
                       id,
                       disabled = false,
                       required = false,
                       onChange = () => null
}) => {
    return (
        <div className={styles.form_item}>
            <label htmlFor={id}>{label}{required && <span>*</span>}</label>
            <input
                value={value}
                id={id}
                type={type}
                placeholder={placeholder}
                onChange={onChange}
                name={name}
                disabled={disabled}
                required={required}
            />
        </div>
    );
};

export default TextInput;