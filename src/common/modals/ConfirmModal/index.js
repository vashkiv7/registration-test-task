import React from 'react';
import styles from './styles.module.scss'
import checkIcon from '../../../assets/img/check_circle.svg'
import {CloseModal} from "../../../assets/img";

const ConfirmModal = ({children, text = [], onClose = () => null }) => {

    return (
        <div className={styles.confirm_modal}>
            <div className={styles.confirm_modal_body}>
                <img src={checkIcon} alt="" className={styles.confirm_modal_body_icon}/>
                <div className={styles.confirm_modal_body_text}>
                    {text.map((el, idx) => <div key={idx}>{el}</div>)}
                </div>
                {children && children}
            </div>
            <div className={styles.confirm_modal_close} onClick={onClose}>
                <CloseModal/>
            </div>
        </div>
    );
};

export default ConfirmModal;