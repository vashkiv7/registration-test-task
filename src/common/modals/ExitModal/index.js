import React from 'react';
import styles from './styles.module.scss'
import exitIcon from '../../../assets/img/exit_circle.svg'
import {CloseModal} from "../../../assets/img";
import SubmitButton from "../../formElements/SubmitButton";

const ExitModal = ({onExit = () => null, onContinue = () => null}) => {
    return (
        <div className={styles.exit_modal}>
            <div className={styles.exit_modal_body}>
                <img src={exitIcon} alt="" className={styles.exit_modal_body_icon}/>
                <div className={styles.exit_modal_body_title}>Подтверждение выхода из аккаунта</div>
                <div className={styles.exit_modal_body_desc}>Вы действительно хотите выйти из своей учетной записи?</div>
                <div className={styles.exit_modal_body_buttons}>
                    <SubmitButton
                        type={"button"}
                        text={"Продолжить"}
                        onClick={onContinue}
                    />
                    <SubmitButton
                        type={"button"}
                        text={"Выйти"}
                        btnStyle={"tertiary"}
                        onClick={onExit}
                    />
                </div>
                <div className={styles.exit_modal_body_close} onClick={onContinue}>
                    <CloseModal/>
                </div>
            </div>
        </div>
    );
};

export default ExitModal;