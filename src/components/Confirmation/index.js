import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from "react-router-dom";
import InputMask from 'react-input-mask'
import styles from './styles.module.scss'
import regStyles from '../Registration/styles.module.scss'
import TextInput from "../../common/formElements/TextInput";
import SubmitButton from "../../common/formElements/SubmitButton";

import timerIcon from '../../assets/img/timer.svg'
import arrowIcon from '../../assets/img/arrow_left.svg'

const Confirmation = () => {
    const navigate = useNavigate()

    const [phoneNumber] = useState(JSON.parse(localStorage.getItem("user"))?.phone)
    const [startTime, setStartTime] = useState(new Date().getTime())
    const [timeLeft, setTimeLeft] = useState(999999)
    const [timeToShow, setTimeToShow] = useState("3")
    const [code, setCode] = useState(null)
    const [isButtonDisabled, setIsButtonDisabled] = useState(true)

    const onInputChange = (event) => {
        setCode(event.target.value)
        setIsButtonDisabled(event.target.value.length !== 6)
    }

    useEffect(() => {
        const timer = () => {
            const diff = new Date().getTime() - startTime
            const left = (1000 * 60 * 3) - diff
            const minLeft = Math.floor((left / (60 * 1000)) % 60);
            const secLeft = Math.floor((left / 1000) % 60);

            if (left < 0) {
                setTimeLeft(0)
                clearInterval(interval)
            } else {
                setTimeLeft(left)
                setTimeToShow(`${minLeft} мин ${secLeft} сек`)
            }
        }

        const interval = setInterval(timer, 1000)
        timer()
        return () => clearInterval(interval)
    }, [startTime])

    const onResendClick = () => {
        setStartTime(new Date().getTime())
    }

    const onFormSubmit = event => {
        event.preventDefault()
        navigate("/registration")
    }

    return (
        <>
            <aside className={styles.aside}>
                <div className={regStyles.aside_title}>Регистрация пользователя</div>
                <div className={regStyles.aside_desc}>Заполните информацию о себе, чтобы начать использовать все преимущества платформы</div>
            </aside>
            <main className={styles.main}>
                <form className={styles.main_form} onSubmit={onFormSubmit}>
                    <div className={styles.main_form_title}>Подтверждение телефона</div>
                    <div className={styles.main_form_desc}>Мы отправили SMS с 6-значным кодом подтверждения на номер {phoneNumber}</div>
                    <InputMask
                        mask="999999"
                        value={code}
                        name={"code"}
                        label={"SMS-код"}
                        placeholder={"Укажите код"}
                        onChange={onInputChange}
                        id={"code_input"}
                        maskChar={null}
                    >
                        {(props) => <TextInput {...props}/>}
                    </InputMask>
                    <div className={styles.main_form_info}>
                        {
                            !!timeLeft ?
                                <div className={styles.main_form_info_timer}>
                                    <img src={timerIcon} alt=""/>{timeToShow}
                                </div> :
                                <div className={styles.main_form_info_resend}
                                    onClick={onResendClick}
                                >Оправить код повторно</div>
                        }
                    </div>
                    <SubmitButton
                        text={"Подтвердить"}
                        disabled={isButtonDisabled}
                    />
                </form>
                <Link to={"/registration"} className={styles.main_back_link}><img src={arrowIcon} alt=""/>Назад</Link>
            </main>
        </>
    );
};

export default Confirmation;