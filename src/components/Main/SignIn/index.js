import React, {useEffect, useState} from 'react';
import TextInput from "../../../common/formElements/TextInput";
import PasswordInput from "../../../common/formElements/PasswordInput";
import SubmitButton from "../../../common/formElements/SubmitButton";

const defaultFormData = {
    email: "",
    password: "",
}

const SignIn = () => {
    const [formData, setFormData] = useState(defaultFormData)
    const [isButtonDisabled, setIsButtonDisabled] = useState(true)

    const onInputChange = event => {
        setFormData(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value
            }
        })
    }

    const checkIsButtonDisabled = () => {
        const mailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,}$/;

        if (!formData.email.match(mailRegex)) {
            setIsButtonDisabled(true)
            return
        }

        if (formData.password.length === 0){
            setIsButtonDisabled(true)
            return
        }

        setIsButtonDisabled(false)
    }

    useEffect(() => {
        checkIsButtonDisabled()
    }, [formData]) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <form>
            <TextInput
                id={"email"}
                type={"email"}
                name={"email"}
                value={formData.email}
                label={"E-mail или телефон"}
                placeholder={"Адрес эл. почты или телефон"}
                onChange={onInputChange}
            />
            <PasswordInput
                name={"password"}
                onChange={onInputChange}
                value={formData.password}
                id={"password"}
                label={"Пароль"}
                placeholder={"Укажите ваш пароль"}
                isGenerator={false}
            />
            <SubmitButton
                type={"submit"}
                disabled={isButtonDisabled}
                text={"Войти в аккаунт"}
            />
        </form>
    );
};

export default SignIn;