import React, {useEffect, useState} from 'react';
import TextInput from "../../../common/formElements/TextInput";
import PasswordInput from "../../../common/formElements/PasswordInput";
import SubmitButton from "../../../common/formElements/SubmitButton";
import {useNavigate} from "react-router-dom";
import ConfirmModal from "../../../common/modals/ConfirmModal";

const defaultFormData = {
    email: "",
    password: "",
    passwordConfirm: ""
}

const LENGTH_ERROR_TEXT = "Длина пароля должна быть не менее 8 и не более 14 символов"
const PASSWORD_STRUCTURE_ERROR_TEXT = "Буквенная часть пароля должна содержать как строчные, так и прописные (заглавные) буквы"
const CONFIRM_PASSWORD_ERROR_TEXT = "Пароли не совпадают"

const SignUp = () => {
    const navigate = useNavigate()

    const [formData, setFormData] = useState(defaultFormData)
    const [isButtonDisabled, setIsButtonDisabled] = useState(true)
    const [passwordError, setPasswordError] = useState(null)
    const [confirmPasswordError, setConfirmPasswordError] = useState(null)
    const [isPasswordGood, setIsPasswordGood] = useState(false)
    const [isConfirmModal, setIsConfirmModal] = useState(false)

    const onInputChange = event => {
        setFormData(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value
            }
        })
    }

    const checkPassword = (password) => {
        const capitalRegex = /[A-Z]/g;
        const smallRegex = /[a-z]/g;
        const numberRegex = /[0-9]/g;

        const isCapital = !!(password.match(capitalRegex))?.length
        const isSmall = !!(password.match(smallRegex))?.length
        const isNumber = !!(password.match(numberRegex))?.length

        const strengthArr = [isCapital, isSmall, isNumber]

        const strengthVal = strengthArr.filter(el => el === true).length

        return strengthVal === 3;
    }

    const checkIsButtonDisabled = () => {
        const mailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,}$/;

        if (!formData.email.match(mailRegex)) {
            setIsButtonDisabled(true)
            return
        }

        if (!checkPassword(formData.password) && (formData.password.length < 8 || formData.password.length > 14)){
            setIsButtonDisabled(true)
            return
        }

        if (formData.password !== formData.passwordConfirm){
            setIsButtonDisabled(true)
            return;
        }

        setIsButtonDisabled(false)
    }

    useEffect(() => {
        checkIsButtonDisabled()

        if (formData.password.length > 0) {
            if (formData.passwordConfirm.length > 0) {
                if (formData.passwordConfirm !== formData.password) {
                    setConfirmPasswordError(CONFIRM_PASSWORD_ERROR_TEXT)
                } else {
                    setConfirmPasswordError(null)
                }
            } else {
                setConfirmPasswordError(null)
            }

            if (formData.password.length < 8 || formData.password.length > 14){
                setPasswordError(LENGTH_ERROR_TEXT)
                setIsPasswordGood(false)
                return
            }

            const valid = checkPassword(formData.password)

            if (!valid) {
                setPasswordError(PASSWORD_STRUCTURE_ERROR_TEXT)
                setIsPasswordGood(false)
                return
            }

            setPasswordError(null)
            setIsPasswordGood(true)
        } else {
            setIsPasswordGood(false)
            setPasswordError(null)
        }

    }, [formData]) // eslint-disable-line react-hooks/exhaustive-deps

    const onFormSubmit = event => {
        event.preventDefault()
        localStorage.setItem("user", JSON.stringify({email: formData.email}))
        setIsConfirmModal(true)
    }

    const onConfirmModalClose = () => {
        navigate("/registration")
    }

    return (
        <>
            <form onSubmit={onFormSubmit}>
                <TextInput
                    id={"email"}
                    type={"email"}
                    name={"email"}
                    value={formData.email}
                    label={"E-mail"}
                    placeholder={"Адрес эл. почты"}
                    onChange={onInputChange}
                />
                <PasswordInput
                    name={"password"}
                    onChange={onInputChange}
                    value={formData.password}
                    id={"password"}
                    label={"Придумайте пароль"}
                    placeholder={"Укажите ваш пароль"}
                    isPasswordGood={isPasswordGood}
                    error={passwordError}
                />
                <PasswordInput
                    name={"passwordConfirm"}
                    onChange={onInputChange}
                    value={formData.passwordConfirm}
                    id={"passwordConfirm"}
                    label={"Повторите пароль"}
                    placeholder={"Повторите ваш пароль"}
                    isGenerator={false}
                    error={confirmPasswordError}
                />
                <SubmitButton
                    type={"submit"}
                    disabled={isButtonDisabled}
                    text={"Зарегистрироваться"}
                />
            </form>
            {
                isConfirmModal && <ConfirmModal
                    text={["Аккаунт был успешно зарегистрирован.", "На ваш E-Mail отправлено письмо с ссылкой для подтверждения"]}
                    onClose={onConfirmModalClose}
                />
            }
        </>
    );
};

export default SignUp;