import React, {useState} from 'react';
import SignUp from "./SignUp";
import SignIn from "./SignIn";

import styles from './styles.module.scss'
import googleIcon from '../../assets/img/google.svg'
import fbIcon from '../../assets/img/facebook.svg'
import linkedinIcon from '../../assets/img/linkedin.svg'
import Dropdown from "../../common/formElements/Dropdown";

const languages = [
    {
        title: "English",
        value: "En"
    },
    {
        title: "Русский",
        value: "Ру"
    },
    {
        title: "Українська",
        value: "Ук"
    },
]

const Main = () => {
    const [isSignIn, setIsSignIn] = useState(false)
    const [selectedLanguage, setSelectedLanguage] = useState(languages[1])

    const onLanguageChange = event => {
        setSelectedLanguage(event.target.value)
    }

    return (
        <>
            <aside className={styles.aside}>
                <div className={styles.aside_title}>Войти в аккаунт</div>
                <div className={styles.aside_desc}>Введите ваш E-mail и пароль, чтобы начать использовать все преимущества платформы:</div>
                <ul className={styles.aside_list}>
                    <li>Автоматизация HR</li>
                    <li>Интеграция с job-порталами</li>
                    <li>Оценка персонала</li>
                    <li>Синхронизация с Outlook</li>
                    <li>Безопасность данных</li>
                    <li>Парсинг резюме</li>
                    <li>Мультиязычность</li>
                    <li>Конструктор отчетности</li>
                </ul>
            </aside>
            <main className={styles.main}>
                <div className={styles.main_lang}>
                    <Dropdown
                        name={"lang"}
                        value={selectedLanguage}
                        items={languages}
                        onChange={onLanguageChange}
                        headClassName={styles.main_lang_head}
                        bodyClassName={styles.main_lang_body}
                        headTitle={"value"}
                    />
                </div>
                <div className={styles.main_content}>
                    <div className={styles.main_content_nav}>
                        <div
                            className={`${styles.main_content_nav_button} ${isSignIn ? styles.active : ""}`}
                            onClick={() => setIsSignIn(true)}
                        >Вход</div>
                        <div
                            className={`${styles.main_content_nav_button} ${isSignIn ? "" : styles.active}`}
                            onClick={() => setIsSignIn(false)}
                        >Регистрация</div>
                    </div>
                    {
                        isSignIn ? <SignIn/> : <SignUp/>
                    }
                    <div className={styles.main_content_social}>
                        <div className={styles.main_content_social_title}>
                            <div></div>
                            <span>Или войдите с помощью</span>
                            <div></div>
                        </div>
                        <div className={styles.main_content_social_row}>
                            <div className={styles.main_content_social_btn}>
                                <img src={googleIcon} alt=""/>
                            </div>
                            <div className={styles.main_content_social_btn}>
                                <img src={fbIcon} alt=""/>
                            </div>
                            <div className={styles.main_content_social_btn}>
                                <img src={linkedinIcon} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
};

export default Main;