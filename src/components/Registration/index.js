import React, {useEffect, useState} from 'react';
import InputMask from 'react-input-mask'
import styles from './styles.module.scss'
import TextInput from "../../common/formElements/TextInput";
import Dropdown from "../../common/formElements/Dropdown";
import SubmitButton from "../../common/formElements/SubmitButton";
import ExitModal from "../../common/modals/ExitModal";

import exitIcon from '../../assets/img/exit.svg'
import {Link, useNavigate} from "react-router-dom";
import ConfirmModal from "../../common/modals/ConfirmModal";

const defaultFormData = {
    firstName: "",
    lastName: "",
    middleName: "",
    dateOfBirth: "",
    sex: null,
    phone: "",
    email: ""
}

const sexItems = [
    {
        value: "male",
        title: "Мужчина",
    },
    {
        value: "female",
        title: "Женщина",
    },
    {
        value: "other",
        title: "Другой",
    },
]

const Registration = () => {
    const navigate = useNavigate()
    const [formData, setFormData] = useState(defaultFormData)
    const [isPhoneCorrect, setIsPhoneCorrect] = useState(false)
    const [isExitModal, setIsExitModal] = useState(false)
    const [isConfirmModal, setIsConfirmModal] = useState(false)

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem("user"))
        setFormData({
            ...formData,
            ...data
        })
        setIsPhoneCorrect(data?.phone?.length === 19)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    const onInputChange = event => {
        if (event.target.name === "phone") {
            setIsPhoneCorrect(event.target.value.length === 19)
        }
        setFormData(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value
            }
        })

        localStorage.setItem("user", JSON.stringify({
            ...formData,
            [event.target.name]: event.target.value
        }))
    }

    const onFormSubmit = event => {
        event.preventDefault()
        if (!formData.sex || formData?.phone?.length !== 19) return
        setIsConfirmModal(true)
    }

    const onRegExit = () => {
        localStorage.removeItem("user")
        navigate("/")
    }

    const onRegContinue = () => {
        setIsExitModal(false)
    }

    return (
        <>
            <aside className={styles.aside}>
                <div className={styles.aside_title}>Регистрация пользователя</div>
                <div className={styles.aside_desc}>Заполните информацию о себе, чтобы начать использовать все преимущества платформы</div>
            </aside>
            <main className={styles.main}>
                <form className={styles.main_form}
                      onSubmit={onFormSubmit}
                >
                    <div className={styles.main_title}>Профиль пользователя</div>
                    <div className={styles.main_form_body}>
                        <div className={styles.main_form_name}>
                            <TextInput
                                value={formData.lastName}
                                name={"lastName"}
                                placeholder={"Михайлов"}
                                label={"Фамилия"}
                                id={"lastName"}
                                onChange={onInputChange}
                                required={true}
                            />
                            <TextInput
                                value={formData.firstName}
                                name={"firstName"}
                                placeholder={"Михаил"}
                                label={"Имя"}
                                id={"firstName"}
                                onChange={onInputChange}
                                required={true}
                            />
                            <TextInput
                                value={formData.middleName}
                                name={"middleName"}
                                placeholder={"Михайлович"}
                                label={"Отчество"}
                                id={"middleName"}
                                onChange={onInputChange}
                                required={true}
                            />
                        </div>
                        <InputMask mask="99/99/9999"
                                   onChange={onInputChange}
                                   value={formData.dateOfBirth}
                                   name={"dateOfBirth"}
                                   placeholder={"10/08/1983"}
                                   label={"Дата рождения"}
                                   id={"dateOfBirth"}
                                   required={true}
                                   maskChar={null}
                        >
                            {(props) => <TextInput {...props}/>}
                        </InputMask>
                        <Dropdown
                            name={"sex"}
                            label={"Пол"}
                            placeholder={"Выберите пол"}
                            value={formData.sex}
                            items={sexItems}
                            onChange={onInputChange}
                            required={true}
                        />
                        <div className={styles.main_form_phone}>
                            <InputMask mask="+99 (999) 999 99 99"
                                       value={formData.phone}
                                       name={"phone"}
                                       placeholder={"+38 (050) 725 60 09"}
                                       label={"Телефон"}
                                       id={"phone"}
                                       type={"tel"}
                                       onChange={onInputChange}
                                       required={true}
                                       maskChar={null}
                            >
                                {(props) => <TextInput{...props}/>}
                            </InputMask>
                            {isPhoneCorrect && <Link to={"/confirmation"} className={styles.main_form_phone_link}>Подтвердить телефон</Link>}
                        </div>
                        <TextInput
                            value={formData.email}
                            name={"email"}
                            placeholder={"E-Mail"}
                            label={"E-Mail"}
                            id={"email"}
                            onChange={onInputChange}
                            disabled={true}
                        />
                    </div>
                    <div className={styles.main_form_bottom}>
                        <div className={styles.main_form_exit}
                             onClick={() => setIsExitModal(true)}
                        >
                            <img src={exitIcon} alt=""/>
                            <span>Выход</span>
                        </div>
                        <SubmitButton
                            text={"Далее"}
                            type={"submit"}
                            disabled={false}
                            className={styles.main_form_next}
                        />
                    </div>
                </form>
            </main>
            {
                isExitModal &&
                <ExitModal
                    onExit={onRegExit}
                    onContinue={onRegContinue}
                />
            }
            {
                isConfirmModal &&
                <ConfirmModal
                    text={["Новая компания успешно создана и добавлена в базу данных. ", "Пройдите верификацию, чтобы завершить регистрацию компании"]}
                    onClose={() => setIsConfirmModal(false)}>
                    <div className={styles.confirm_buttons}>
                        <SubmitButton
                            text={"Пройти верификацию"}
                            onClick={onRegExit}
                            type={"button"}
                            btnStyle={"secondary"}
                            className={styles.confirm_buttons_verify}
                        />
                        <SubmitButton
                            text={"Перейти в Профиль Компаний"}
                            onClick={onRegExit}
                            type={"button"}
                            btnStyle={"tertiary"}
                            className={styles.confirm_buttons_to_profile}
                        />
                    </div>
                </ConfirmModal>
            }
        </>
    );
};

export default Registration;