import {Route, Routes} from "react-router-dom";
import Main from "../components/Main";
import Registration from "../components/Registration";
import Confirmation from "../components/Confirmation";

const Router = () => {
    return(
        <Routes>
            <Route path="/" element={<Main/>}/>
            <Route path="/registration" element={<Registration/>}/>
            <Route path="/confirmation" element={<Confirmation/>}/>
        </Routes>
    )
}

export default Router